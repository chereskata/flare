use gdk::glib::{Priority, Propagation};
use glib::{
    clone,
    subclass::{InitializingObject, Signal},
};
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::subclass::widget::WidgetImpl;
use gtk::{gdk, gio, glib, CompositeTemplate, TemplateChild, TextView};
use once_cell::sync::Lazy;

use crate::gspawn;
use crate::gui::utility::Utility;

use std::cell::Cell;
use std::marker::PhantomData;

glib::wrapper! {
    pub struct TextEntry(ObjectSubclass<imp::TextEntry>)
        @extends gtk::Box, gtk::Widget,
        @implements gtk::gio::ActionGroup, gtk::gio::ActionMap, gtk::Accessible, gtk::Buildable,
            gtk::ConstraintTarget;
}

impl TextEntry {
    pub fn buffer(&self) -> sourceview5::Buffer {
        self.imp().view.buffer().downcast().unwrap()
    }

    pub fn text(&self) -> String {
        let buffer: sourceview5::Buffer = self.buffer();
        let (start_iter, end_iter) = buffer.bounds();
        buffer.text(&start_iter, &end_iter, true).to_string()
    }

    pub fn set_text(&self, text: String) {
        let buffer: sourceview5::Buffer = self.buffer();
        buffer.set_text(text.as_str());
    }

    pub fn clear(&self) {
        let buffer: sourceview5::Buffer = self.buffer();
        buffer.set_text("");
    }

    pub fn insert_emoji(&self) {
        self.imp().insert_emoji();
    }
}

pub mod imp {

    use super::*;

    #[derive(CompositeTemplate, Default, glib::Properties)]
    #[template(resource = "/ui/text_entry.ui")]
    #[properties(wrapper_type = super::TextEntry)]
    pub struct TextEntry {
        #[template_child]
        pub(super) view: TemplateChild<TextView>,
        #[property(get, set)]
        send_on_enter: Cell<bool>,
        #[property(get = Self::is_empty)]
        _is_empty: PhantomData<bool>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for TextEntry {
        const NAME: &'static str = "FlTextEntry";
        type Type = super::TextEntry;
        type ParentType = gtk::Box;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_callbacks();
            klass.set_css_name("message-entry");
            Utility::bind_template_callbacks(klass);
        }

        fn instance_init(obj: &InitializingObject<Self>) {
            obj.init_template();
        }
    }
    #[gtk::template_callbacks]
    impl TextEntry {
        #[template_callback]
        pub(super) fn insert_emoji(&self) {
            self.view.emit_insert_emoji();
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for TextEntry {
        fn constructed(&self) {
            let obj = self.obj();
            let key_events = gtk::EventControllerKey::new();
            key_events
                .connect_key_pressed(clone!(@weak obj => @default-return Propagation::Proceed, move |_, key, _, modifier| {
                let enter_pressed = key == gdk::Key::Return || key == gdk::Key::KP_Enter;
                let should_send = enter_pressed && if obj.send_on_enter() {
                    // shift+enter / ctrl+enter yields newline, enter sends
                    !modifier.contains(gdk::ModifierType::CONTROL_MASK) && !modifier.contains(gdk::ModifierType::SHIFT_MASK)
                } else {
                    // enter / shift+enter yields newline, ctrl+enter sends
                    modifier.contains(gdk::ModifierType::CONTROL_MASK)
                };
                if should_send {
                    obj.emit_by_name::<()>("activate", &[]);
                    Propagation::Stop
                } else {
                    Propagation::Proceed
                }
            }));
            self.view.add_controller(key_events);

            self.view
                .connect_paste_clipboard(clone!(@weak obj => move |entry| {
                    let clipboard = obj.clipboard();
                    let formats = clipboard.formats();

                    // We only handle files and supported images.
                    gspawn!(clone!(@weak entry => async move {
                        if formats.contains_type(gio::File::static_type()) {
                            entry.stop_signal_emission_by_name("paste-clipboard");
                            match clipboard
                                .read_value_future(gio::File::static_type(), Priority::DEFAULT)
                                .await
                            {
                                Ok(value) => match value.get::<gio::File>() {
                                    Ok(file) => {
                                        obj.emit_by_name::<()>("paste-file", &[&file]);
                                    }
                                    Err(error) => log::warn!("Could not get file from value: {error:?}"),
                                },
                                Err(error) => log::warn!("Could not get file from the clipboard: {error:?}"),
                            }
                        } else if formats.contains_type(gdk::Texture::static_type()) {
                            entry.stop_signal_emission_by_name("paste-clipboard");
                            match clipboard
                                .read_value_future(gdk::Texture::static_type(), Priority::DEFAULT)
                                .await
                            {
                                Ok(value) => match value.get::<gdk::Texture>() {
                                    Ok(texture) => {
                                        obj.emit_by_name::<()>("paste-texture", &[&texture]);
                                    }
                                    Err(error) => log::warn!("Could not get file from value: {error:?}"),
                                },
                                Err(error) => log::warn!("Could not get file from the clipboard: {error:?}"),
                            }
                        }
                    }));
                }));

            let buffer = sourceview5::Buffer::new(None);
            obj.imp().view.set_buffer(Some(&buffer));
            buffer.connect_text_notify(clone!(@weak obj => move |_| {
                obj.notify_is_empty();
            }));

            #[cfg(feature = "libspelling")]
            {
                let checker = libspelling::Checker::default();
                let adapter = libspelling::TextBufferAdapter::new(&buffer, &checker);
                let extra_menu = adapter.menu_model();

                self.view.set_extra_menu(Some(&extra_menu));
                self.view.insert_action_group("spelling", Some(&adapter));

                adapter.set_enabled(true);
            }
        }

        fn signals() -> &'static [Signal] {
            static SIGNALS: Lazy<Vec<Signal>> = Lazy::new(|| -> Vec<Signal> {
                vec![
                    Signal::builder("activate").build(),
                    Signal::builder("paste-file")
                        .param_types([gio::File::static_type()])
                        .build(),
                    Signal::builder("paste-texture")
                        .param_types([gdk::Texture::static_type()])
                        .build(),
                ]
            });
            SIGNALS.as_ref()
        }
    }

    impl WidgetImpl for TextEntry {
        fn grab_focus(&self) -> bool {
            log::trace!("TextEntry grabbed focus");
            self.view.grab_focus()
        }
    }
    impl BoxImpl for TextEntry {}

    impl TextEntry {
        fn is_empty(&self) -> bool {
            let (start, end) = self.obj().buffer().bounds();
            start == end
        }
    }
}
